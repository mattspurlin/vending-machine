require 'simplecov'
SimpleCov.start
require 'rspec'
require 'change_machine'

describe ChangeMachine do
  before(:each) do
    @change_machine = ChangeMachine
  end

  describe '.get_coin_value' do
    it 'should return 0.25 when given a quarter' do
      expect(@change_machine.get_coin_value(:quarter)).to eq 0.25
    end
  end

  describe '.round' do
    it 'should round 0.4999999 to 50' do
      expect(@change_machine.round(0.4999999)).to eq 50
    end
  end

  describe '.get_largest_coin_for_value' do
    it 'should return :nickel given 0.07' do
      expect(@change_machine.get_largest_coin_for_value(0.07)).to eq :nickel
    end
  end

  describe '.get_optimal_change_return' do
    it 'should return 2 quarters given 0.50' do
      cents = [:quarter, :quarter]
      expect(@change_machine.get_optimal_change_return(0.50)).to eq cents
    end
  end

  describe '.get_value_for_set_of_coins' do
    it 'should return 0.65 for an array of 2 quarters, a nickel and a dime' do
      cents = [:quarter, :quarter, :nickel, :dime]
      expect(@change_machine.get_value_for_coins(cents)).to eq 0.65
    end
  end

  describe '.can_make_change?' do
    it 'should return false if unable to make change with current coins' do
      cents = [:quarter, :quarter, :quarter]
      expect(@change_machine.can_make_change?(0.60, cents)).to eq false
    end
  end
end
