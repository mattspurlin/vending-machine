require 'simplecov'
SimpleCov.start
require 'rspec'
require 'vending_machine'

describe VendingMachine do
  before(:each) do
    @vending_machine = VendingMachine.new
  end

  describe '#insert' do
    it 'should display "INSERT COIN" when no coins are inserted' do
      expect(@vending_machine.display).to eq 'INSERT COIN'
    end

    it 'should display the string 0.25 when a quarter has been inserted' do
      @vending_machine.insert :quarter
      expect(@vending_machine.display).to eq '0.25'
    end

    it 'should display the string 0.50 when 2 quarters have been inserted' do
      2.times do
        @vending_machine.insert :quarter
      end
      expect(@vending_machine.display).to eq '0.50'
    end

    it 'should accepts nickels and properly return the value in the display' do
      @vending_machine.insert :nickel
      expect(@vending_machine.display).to eq '0.05'
    end

    it 'should accept a dime and return the display as "0.10"' do
      @vending_machine.insert :dime
      expect(@vending_machine.display).to eq '0.10'
    end

    it 'should not increase the value on the display when given a penny' do
      @vending_machine.insert :penny
      expect(@vending_machine.display).to eq 'INSERT COIN'
    end

    it 'should put a penny in the coin return when inserted' do
      @vending_machine.insert :penny
      expect(@vending_machine.coin_return).to eq [:penny]
    end
  end

  describe '#press_button' do
    it 'should display item price when button pressed and no money inserted' do
      @vending_machine.press_button :chips
      expect(@vending_machine.display).to eq '0.50'
    end

    it 'should display correct item price for candy without money' do
      @vending_machine.press_button :candy
      expect(@vending_machine.display).to eq '0.65'
    end

    it 'should display 1.00 for cola without money inserted' do
      @vending_machine.press_button :cola
      expect(@vending_machine.display).to eq '1.00'
    end

    it 'should display 1.00 followed by INSERT COIN when checked twice' do
      @vending_machine.press_button :cola
      expect(@vending_machine.display).to eq '1.00'
      expect(@vending_machine.display).to eq 'INSERT COIN'
    end

    context '1.00 inserted' do
      before(:each) do
        10.times do
          @vending_machine.insert :dime
        end
      end

      it 'should display THANK YOU when appropriate amount inserted' do
        @vending_machine.press_button :candy
        expect(@vending_machine.display).to eq 'THANK YOU'
      end

      it 'the display resets when proper amount inserted and display checked' do
        @vending_machine.press_button :chips
        expect(@vending_machine.display).to eq 'THANK YOU'
        expect(@vending_machine.display).to eq 'INSERT COIN'
      end

      it 'dispenses a product when proper amount inserted' do
        @vending_machine.press_button :cola
        expect(@vending_machine.product_receptacle).to eq ['Cola']
      end

      it 'dispenses correct change when inserted amount more than cost' do
        @vending_machine.press_button :chips
        expect(@vending_machine.coin_return).to eq [:quarter, :quarter]
      end

      it 'shows sold out when no more cola is available' do
        @vending_machine.press_button :cola
        expect(@vending_machine.display).to eq 'THANK YOU'
        4.times do
          @vending_machine.insert :quarter
        end
        @vending_machine.press_button :cola
        expect(@vending_machine.display).to eq 'SOLD OUT'
        expect(@vending_machine.display).to eq '1.00'
        @vending_machine.press_return_coins
        @vending_machine.press_button :cola
        expect(@vending_machine.display).to eq 'SOLD OUT'
        expect(@vending_machine.display).to eq 'INSERT COIN'
      end
    end

    context '0.75 inserted' do
      before(:each) do
        3.times do
          @vending_machine.insert :quarter
        end
      end

      it 'shows "EXACT CHANGE ONLY" when it cannot make change' do
        @vending_machine.press_button :candy
        expect(@vending_machine.display).to eq 'EXACT CHANGE ONLY'
      end
    end
  end

  describe '#press_creturn_coins' do
    context '1.00 inserted' do
      before(:each) do
        4.times do
          @vending_machine.insert :quarter
        end
      end

      it 'returns four quarters' do
        coins = [:quarter, :quarter, :quarter, :quarter]
        @vending_machine.press_return_coins
        expect(@vending_machine.coin_return).to eq coins
        expect(@vending_machine.display).to eq 'INSERT COIN'
      end
    end
  end
end
