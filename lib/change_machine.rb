# A Machine to calculate various attributes of change
class ChangeMachine
  # returns whether or not change can be made given a purchase amount
  # and current available coins
  def self.can_make_change?(amount, bank)
    return true if amount == 0.0
    possibilities = []
    (1..bank.length).each do |num|
      possibilities += bank.combination(num).to_a
    end
    possibilities.each do |poss_array|
      value = get_value_for_coins poss_array
      return true if poss_array.all? { |e| bank.include?(e) } && value == amount
    end
    false
  end

  # returns a value as a float given a coin the vending machines knows about
  def self.get_coin_value(coin)
    case coin
    when :quarter
      0.25
    when :dime
      0.10
    when :nickel
      0.05
    end
  end

  # given an amount return the largest coin under that amount
  def self.get_largest_coin_for_value(amount)
    if round(amount) >= round(get_coin_value(:quarter))
      :quarter
    elsif round(amount) >= round(get_coin_value(:dime))
      :dime
    elsif round(amount) >= round(get_coin_value(:nickel))
      :nickel
    end
  end

  # returns an array of change that optimally matches the amount needed
  # assumes nickels are the smalled coins that can be returned
  def self.get_optimal_change_return(amount)
    return [] unless (round(amount) % round(get_coin_value(:nickel))) == 0
    change = get_largest_coin_for_value amount
    amount -= get_coin_value change if change
    if round(amount) > 0
      [change] + get_optimal_change_return(amount)
    else
      [change]
    end
  end

  # given an array of coins return the total value of that array
  def self.get_value_for_coins(coins)
    value = 0.0
    coins.each do |coin|
      value += get_coin_value coin
    end
    round(value) / 100.0
  end

  # rounds a float to 2 places and converts it to an integer
  # also multiplies by 100 so no digits are lost when converting
  # this is used when precision is needed with floats
  def self.round(float)
    (float.round(2) * 100).to_i
  end
end
