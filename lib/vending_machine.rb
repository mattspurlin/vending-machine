require 'change_machine'

# A simple Vending Machine with the ability to accepts coins and dispense goods
class VendingMachine
  attr_reader :coin_return
  attr_reader :product_receptacle

  def initialize
    @bank = [:nickel, :nickel]
    @change_machine = ChangeMachine
    @coin_return = []
    @current_display = nil
    @current_coins = []
    @product_receptacle = []
    @products = { cola: 1, chips: 2, candy: 3 }
  end

  # takes a float and converts to a standard US dollar format
  def convert_float_to_dollars(float)
    dollars, cents = float.to_s.split '.'
    cents = "#{cents}0" if cents.length == 1
    "#{dollars}.#{cents}"
  end

  # dispense a product
  def dispense_product(button)
    @product_receptacle.push(get_product_for_button(button))
  end

  # display will return a string of the current value of coins inserted
  # unless no coins have been inserted
  def display
    current_value = @change_machine.get_value_for_coins @current_coins
    unless @current_display
      return 'INSERT COIN' unless current_value > 0
      return convert_float_to_dollars(current_value)
    end
    temp_display = @current_display
    @current_display = nil
    temp_display
  end

  # returns a value as a float for a given button selection
  def get_price_for_button(button)
    case button
    when :cola
      1.00
    when :chips
      0.50
    when :candy
      0.65
    end
  end

  # returns the corresponding product as a string given a button
  def get_product_for_button(button)
    case button
    when :cola
      'Cola'
    when :chips
      'Chips'
    when :candy
      'Candy'
    end
  end

  # insert expects coins in the form of symbols.
  # The symbols are then inspected to find what type of coin they are.
  def insert(coin)
    coin_value = @change_machine.get_coin_value coin
    if coin_value
      @current_coins.push coin
    else
      @coin_return.push coin
    end
  end

  # attempts to vend appropriate item when button is pressed
  # rubocop:disable MethodLength
  def press_button(button)
    price = get_price_for_button button
    current_amount = @change_machine.get_value_for_coins(@current_coins)
    available_coins = @current_coins + @bank
    change = current_amount - price
    @current_display = if @products[button] == 0
                         'SOLD OUT'
                       elsif price > current_amount
                         convert_float_to_dollars price
                         # rubocop:disable LineLength
                       elsif @change_machine.can_make_change?(change, available_coins)
                         # rubocop:enable LineLength
                         vend button
                         'THANK YOU'
                       else
                         'EXACT CHANGE ONLY'
                       end
  end
  # rubocop:enable MethodLength

  # returns unspent coins
  def press_return_coins
    @coin_return += @current_coins
    @current_coins = []
  end

  # return the correct amount of coins given a current amount of inserted coins
  # and the price associated with the pressed button
  def return_coins(button)
    price = get_price_for_button button
    return_amount = @change_machine.get_value_for_coins(@current_coins) - price
    coins_to_return = @change_machine.get_optimal_change_return return_amount
    @coin_return += coins_to_return if coins_to_return
    @current_coins = []
  end

  # performs vend actions relating to a button
  def vend(button)
    @products[button] -= 1
    return_coins button
    dispense_product button
  end
end
